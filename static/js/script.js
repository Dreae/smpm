document.addEventListener("DOMContentLoaded", (event) => {
    let handle_filename = (id_part) => {
        let input = document.getElementById(id_part);
        if (input) {
            input.onchange = () => {
                if (input.files.length > 0) {
                    document.getElementById(id_part + '_name').innerHTML = input.files[0].name;
                }
            }
        }
    }
    handle_filename('plugin_file');
    handle_filename('plugin_archive');
});