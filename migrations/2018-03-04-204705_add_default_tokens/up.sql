CREATE TABLE default_tokens (
    user_id INT NOT NULL REFERENCES users(user_id),
    token_id VARCHAR(32) NOT NULL REFERENCES access_tokens(key_id),
    PRIMARY KEY (user_id, token_id)
);

INSERT INTO default_tokens (token_id, user_id) SELECT key_id as token_id, user_id FROM access_tokens LIMIT 1;
UPDATE access_tokens SET token_desc = 'Default access token' WHERE key_id in (SELECT token_id FROM default_tokens);