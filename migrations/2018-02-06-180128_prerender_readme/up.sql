CREATE TABLE version_readme (
    readme_id SERIAL PRIMARY KEY,
    version_id INT NOT NULL REFERENCES versions(version_id),
    readme TEXT NOT NULL,
    date_rendered TIMESTAMP NOT NULL DEFAULT NOW()
)