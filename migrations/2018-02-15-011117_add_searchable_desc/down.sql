ALTER TABLE plugins DROP COLUMN description;
ALTER TABLE plugins DROP COLUMN desc_searchable_col;
DROP INDEX IF EXISTS idx_plugin_desc;
DROP TRIGGER desc_tsvectorupdate ON plugins;