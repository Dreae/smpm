ALTER TABLE plugins ADD COLUMN description TEXT NOT NULL DEFAULT '';
ALTER TABLE plugins ADD COLUMN desc_searchable_col tsvector NOT NULL DEFAULT to_tsvector('english', '');
UPDATE plugins SET desc_searchable_col = to_tsvector(description);
CREATE INDEX idx_plugin_desc ON plugins USING GIN (desc_searchable_col);

CREATE TRIGGER desc_tsvectorupdate BEFORE INSERT OR UPDATE 
ON plugins FOR EACH ROW EXECUTE PROCEDURE
tsvector_update_trigger(desc_searchable_col, 'pg_catalog.english', description);