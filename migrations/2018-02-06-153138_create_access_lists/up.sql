CREATE TABLE access_tokens (
    key_id VARCHAR(32) NOT NULL PRIMARY KEY,
    token VARCHAR(256) NOT NULL UNIQUE,
    user_id INT NOT NULL REFERENCES users(user_id)
);

CREATE TABLE access_list (
    user_id INT NOT NULL REFERENCES users(user_id),
    plugin_id INT NOT NULL REFERENCES plugins(plugin_id),
    PRIMARY KEY(user_id, plugin_id)
);