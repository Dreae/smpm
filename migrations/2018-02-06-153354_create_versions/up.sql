CREATE TABLE versions (
    version_id SERIAL PRIMARY KEY,
    num VARCHAR(16) NOT NULL,
    plugin_id INT NOT NULL REFERENCES plugins(plugin_id),
    created_date TIMESTAMP NOT NULL DEFAULT NOW()
);