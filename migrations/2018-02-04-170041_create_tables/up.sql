CREATE TABLE users (
    user_id SERIAL PRIMARY KEY,
    username VARCHAR(32) NOT NULL,
    email VARCHAR(128) NOT NULL,
    password VARCHAR(256) NOT NULL
);

CREATE TABLE plugins (
    plugin_id SERIAL PRIMARY KEY,
    name VARCHAR(64) NOT NULL UNIQUE,
    user_id INT NOT NULL REFERENCES users(user_id),
    private BOOLEAN NOT NULL DEFAULT FALSE,
    updated_date TIMESTAMP NOT NULL DEFAULT NOW(),
    created_date TIMESTAMP NOT NULL DEFAULT NOW(),
    downloads INT NOT NULL DEFAULT 0,
    max_version VARCHAR(16) NOT NULL DEFAULT '0.0.0'
);

CREATE INDEX idx_plugin_name ON plugins(name);
CREATE INDEX idx_plugin_created ON plugins(created_date);
CREATE INDEX idx_plugin_updated ON plugins(updated_date);
CREATE INDEX idx_plugin_downloads ON plugins(downloads);