DROP INDEX idx_plugin_name;
DROP INDEX idx_plugin_created;
DROP INDEX idx_plugin_updated;
DROP INDEX idx_plugin_downloads;

DROP TABLE plugins;
DROP TABLE users;