#include "Extension.hpp"
#include "RootCommand.hpp"

Extension extension;
SMEXT_LINK(&extension);

bool Extension::SDK_OnLoad(char *error, size_t err_max, bool late) {
    rootconsole->AddRootConsoleCommand3("pm", "Manage SourceMod plugins", &rootCommand);
    init_plugins();
    return true;
}

void Extension::SDK_OnUnload() {
  
}
