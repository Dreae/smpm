#ifndef INC_SEXT_ROOTCOMMAND_H
#define INC_SEXT_ROOTCOMMAND_H

#include "Extension.hpp"

class RootCommand : public IRootConsoleCommand {
public:
    void OnRootConsoleCommand(const char *cmdname, const ICommandArgs *args);
};

extern RootCommand rootCommand;

#endif