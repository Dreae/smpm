#include "RootCommand.hpp"

RootCommand rootCommand;

void RootCommand::OnRootConsoleCommand(const char *cmdname, const ICommandArgs *args) {
    root_command(args->ArgS());
}