#include "rust_glue.hpp"

void console_print(const char* msg) {
    rootconsole->ConsolePrint("%s", msg);
}

void console_log(const char* msg) {
    LOG_MESSAGE("%s", msg);
}

const char *get_sourcemod_path() {
    return smutils->GetSourceModPath();
}