use toml;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;

use std::io::{Error as IOError};
use toml::de::{Error as TomlError};

#[derive(Serialize, Deserialize)]
pub struct SmpmConfig {
   plugins: Vec<PluginConfig>,
}

#[derive(Serialize, Deserialize)]
pub struct PluginConfig {
    auto_update: bool,
    name: String,
    url: String,
    smx_files: Vec<String>,
}

impl SmpmConfig {
    fn new() -> SmpmConfig {
        SmpmConfig {
            plugins: vec![],
        }
    }
}

pub fn read_config(sm_path: &str) -> Result<SmpmConfig, ConfigError> {
    let file = File::open(&format!("{}/configs/smpm.toml", sm_path))?;
    let mut buf_reader = BufReader::new(file);
    let mut contents = String::new();
    buf_reader.read_to_string(&mut contents)?;

    Ok(toml::from_str(&contents)?)
}

pub fn make_config_file(sm_path: &str) -> Result<(), IOError> {
    match File::create(&format!("{}/configs/smpm.toml", sm_path)) {
        Ok(mut file) => {
            let config = SmpmConfig::new();
            let buf = toml::ser::to_vec(&config).unwrap();
            file.write(&buf)?;

            Ok(())
        },
        Err(error) => Err(error),
    }
}

pub enum ConfigError {
    FileError(IOError),
    ParseError(TomlError),
}

impl From<IOError> for ConfigError {
    fn from(error: IOError) -> ConfigError {
        ConfigError::FileError(error)
    }
}

impl From<TomlError> for ConfigError {
    fn from(error: TomlError) -> ConfigError {
        ConfigError::ParseError(error)
    }
}