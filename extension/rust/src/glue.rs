use std::ffi::{CString, CStr};
use std::os::raw::c_char;

pub fn console_print(msg: &str) {
    unsafe {
        c::console_print(CString::new(msg).unwrap().as_ptr())
    }
}

pub fn console_log(msg: &str) {
    unsafe {
        c::console_log(CString::new(msg).unwrap().as_ptr())
    }
}

pub fn get_sourcemod_path() -> String {
    unsafe {
        let c_str = CStr::from_ptr(c::get_sourcemod_path());
        
        // Clone out of C so we can make guarantees about lifetime
        c_str.to_owned().into_string().unwrap()
    }
}

mod c {
    use super::c_char;

    extern "C" {
        pub fn console_print(msg: *const c_char);
        pub fn console_log(msg: *const c_char);
        pub fn get_sourcemod_path() -> *const c_char;
    }
}