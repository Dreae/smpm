#[macro_use] extern crate clap;
#[macro_use] extern crate serde_derive;
extern crate reqwest;
extern crate toml;

use std::os::raw::c_char;
use std::ffi::CStr;
use std::io::ErrorKind;

mod glue;
mod config;

use config::ConfigError;

#[no_mangle]
pub extern "C" fn root_command(arg_string: *const c_char) {
    let app = clap_app!(smpm => 
        (about: "Manage SourceMod plugins")
        (@subcommand install =>
            (about: "Install a SourceMod plugin")
            (@arg auto_update: -a --auto_update "Automatically update the installed plugin")
            (@arg PLUGIN: +required "Name of the plugin to install")
        )
    );
    let args = unsafe { CStr::from_ptr(arg_string) };
    match app.get_matches_from_safe(args.to_string_lossy().split(' ')) {
        Err(clap_error) => {
            glue::console_print(&clap_error.message);
        },
        Ok(matches) => {
            if let Some(matches) = matches.subcommand_matches("install") {
                install::run_command(matches);
            } else {
                glue::console_print("You must enter a subcommand. Use 'sm pm help' for info");
            }
        }
    };
}


#[no_mangle]
pub extern "C" fn init_plugins() {
    glue::console_log("SourceMod Plugin Manager is initializing");
    
    let sm_path = glue::get_sourcemod_path();
    glue::console_log(&format!("SourceMod directory is {}", glue::get_sourcemod_path()));
    match config::read_config(&sm_path) {
        Ok(_config) => { },
        Err(error) => {
            match error {
                ConfigError::FileError(file_error) => {
                    if file_error.kind() == ErrorKind::NotFound {
                        if let Err(error) = config::make_config_file(&sm_path) {
                            glue::console_log(&format!("Error creating config file: {:?}", error));
                        }                        
                    } else {
                        glue::console_log(&format!("Error reading config file: {}", file_error));
                    }
                },
                ConfigError::ParseError(toml_error) => {
                    glue::console_log(&format!("Error parsing config file: {}", toml_error));
                }
            }
        }
    };
}