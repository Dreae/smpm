BEGIN;
    INSERT INTO users (username, password, email) values ('dreae', '6e877186673821450aa6b1d2f2a4b9c31cf5e099544de290165cda4e39f65a92:9589f05e49c98cf89ac5e5cfae53627f', 'foo@foo.com');
    INSERT INTO access_tokens (user_id, key_id, token) values (CURRVAL('users_user_id_seq'::regclass), 'dc120007dfa1563b', '3cac2b238f212cc98bd16252d4d334236b4ffc834e7ad59f0ef4b2f5906a418f');
    INSERT INTO plugins (name, user_id, private, max_version, description, created_date, updated_date) values ('test', CURRVAL('users_user_id_seq'::regclass), FALSE, '1.0.2', 'This is a test plugin', TIMESTAMP '2018-02-05 10:23:54', TIMESTAMP '2018-02-06 10:23:54');
    INSERT INTO versions (num, plugin_id, created_date) values ('1.0.0', CURRVAL('plugins_plugin_id_seq'::regclass), TIMESTAMP '2018-02-05 10:23:54');
    INSERT INTO version_readme (version_id, readme) values (CURRVAL('versions_version_id_seq'::regclass), '<h2>Test Plugin</h2><p>This plugin is a <strong>test</strong></p>');
    INSERT INTO versions (num, plugin_id, created_date) values ('1.0.2', CURRVAL('plugins_plugin_id_seq'::regclass), TIMESTAMP '2018-02-06 10:23:54');
    INSERT INTO version_readme (version_id, readme) values (CURRVAL('versions_version_id_seq'::regclass), '<h2>Test Plugin</h2><p>This plugin is a <strong>test</strong></p>');

    INSERT INTO plugins (name, user_id, private, max_version, created_date, updated_date) values ('test2', CURRVAL('users_user_id_seq'::regclass), FALSE, '1.0.0', TIMESTAMP '2018-02-06 10:23:50', TIMESTAMP '2018-02-06 10:23:50');
    INSERT INTO versions (num, plugin_id) values ('1.0.0', CURRVAL('plugins_plugin_id_seq'::regclass));
    INSERT INTO version_readme (version_id, readme) values (CURRVAL('versions_version_id_seq'::regclass), '<h2>Test Plugin</h2><p>This plugin is a <strong>test</strong></p>');

    INSERT INTO plugins (name, user_id, private, max_version, description, created_date, updated_date) values ('test private', CURRVAL('users_user_id_seq'::regclass), TRUE, '1.0.0', 'This is a private test plugin', TIMESTAMP '2018-02-06 10:23:50', TIMESTAMP '2018-02-06 10:23:50');
    INSERT INTO versions (num, plugin_id) values ('1.0.0', CURRVAL('plugins_plugin_id_seq'::regclass));
    INSERT INTO version_readme (version_id, readme) values (CURRVAL('versions_version_id_seq'::regclass), '<h2>Test Plugin</h2><p>This plugin is a <strong>test</strong></p>');
COMMIT;