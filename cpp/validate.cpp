#include "environment.h"
#include "method-verifier.h"
#include <set>
#include <deque>
#include <string.h>
#include <stdio.h>

using namespace sp;

#define VERIFY_ERROR_NONE    0
#define VERIFY_ERROR_LOAD    1
#define VERIFY_ERROR_VERIFY  2
#define VERIFY_ERROR_MYINFO  3
#define VERIFY_ERROR_VERSION 4

static bool verify(IPluginRuntime *rt, char *buffer) {
    std::set<cell_t> seen;
    std::deque<cell_t> work;
    for (size_t i = 0; i < rt->GetPublicsNum(); i++) {
        int err;
        sp_public_t *fun;
        if ((err = rt->GetPublicByIndex(i, &fun)) != SP_ERROR_NONE) {
            sprintf(buffer, "Could not get public function at index %zu\n", i);
        }

        seen.insert(fun->code_offs);
        work.push_back(fun->code_offs);
    }

    auto onExternFuncRef = [&seen, &work](cell_t offset) -> void {
        if (seen.find(offset) != seen.end()) {
            return;
        }

        seen.insert(offset);
        work.push_back(offset);
    };

    while (!work.empty()) {
        auto offset = work.front();
        work.pop_front();

        MethodVerifier verifier(PluginRuntime::FromAPI(rt), offset);
        verifier.collectExternalFuncRefs(onExternFuncRef);
        if (!verifier.verify()) {
            sprintf(buffer, "Failed verification: %d\n", verifier.error());
            return false;
        }
    }

    return true;
}

extern "C" int try_get_plugin_version(Environment *sEnv, const char *file, char *buffer, int buffer_size) {
    AutoPtr<IPluginRuntime> rt(sEnv->APIv2()->LoadBinaryFromFile(file, buffer, buffer_size));
    if (!rt) {
        return VERIFY_ERROR_LOAD;
    }

    if (!verify(rt, buffer)) {
        return VERIFY_ERROR_VERIFY;
    }

    uint32_t index;
    if (rt->FindPubvarByName("myinfo", &index) != SP_ERROR_NONE) {
        return VERIFY_ERROR_MYINFO;
    }

    cell_t local_addr;
    cell_t *info;
    IPluginContext *cx = rt->GetDefaultContext();
    cx->GetPubvarAddrs(index, &local_addr, &info);

    char *str;
    if (cx->LocalToString(info[3], &str) != SP_ERROR_NONE) {
        return VERIFY_ERROR_VERSION;
    }
    strcpy(buffer, str);

    return VERIFY_ERROR_NONE;
}

extern "C" Environment *init_sourcepawn_environment() {
    return Environment::New();
}