#[derive(Serialize, Deserialize)]
pub struct PluginManifest {
    pub plugin_id: i32,
    pub name: String,
    pub version: String,
    pub author: String,
    pub versions: Vec<String>
}

#[derive(Serialize, Deserialize)]
pub struct PluginVersionHistory {
    pub versions: Vec<PluginVersionSummary>
}

#[derive(Serialize, Deserialize)]
pub struct PluginVersionSummary {
    pub num: String,
    pub version: String
}

#[derive(Serialize, Deserialize, PartialEq)]
pub enum ArtifactType {
    Archive,
    Plugin
}

impl ArtifactType {
    pub fn into_int(&self) -> i32 {
        match self {
            &ArtifactType::Archive => 0,
            &ArtifactType::Plugin => 1
        }
    }
}

impl From<i32> for ArtifactType {
    fn from(int: i32) -> ArtifactType {
        match int {
            0 => ArtifactType::Archive,
            _ => ArtifactType::Plugin
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct PluginVersion {
    pub id: i32,
    pub num: String,
    pub artifact_type: ArtifactType,
    pub artifact: String,
    pub date: i64
}