#[derive(Serialize, Deserialize)]
pub struct ArchiveManifest {
    pub name: String,
    pub description: String,
    pub version: String,
    pub private: bool,
    pub plugin_files: Vec<String>
}