use std::ffi::CString;
use std::sync::{Arc, Mutex};
use std::os::raw::c_void;
use std::ptr::Unique;

type Environment = c_void;

#[derive(Debug)]
pub enum PluginValidationError {
    ErrorCode(i32, String)
}

lazy_static! {
    // This terrible incantation is because only one sp::Environment is allowed per process,
    // and it's probably not thread safe
    static ref SOURCEPAWN_ENV: Arc<Mutex<Unique<Environment>>> = Arc::new(Mutex::new(Unique::new(unsafe { c::init_sourcepawn_environment() }).unwrap()));
}

pub fn try_get_plugin_version(plugin: &str) -> Result<String, PluginValidationError> {
    let mut buffer = Vec::with_capacity(255);
    let result = unsafe {
        buffer.set_len(255);
        let s_env = SOURCEPAWN_ENV.lock().unwrap().as_ptr();
        c::try_get_plugin_version(s_env, CString::new(plugin).unwrap().as_ptr(), buffer.as_mut_ptr() as *mut i8, 255)
    };

    let valid_str = buffer.into_iter().take_while(|c| c != &0).collect();

    let msg = String::from_utf8(valid_str).unwrap();

    if result != 0 {
        Err(PluginValidationError::ErrorCode(result, msg))
    } else {
        Ok(msg)
    }

}

mod c {
    use std::os::raw::c_char;
    extern "C" {
        pub fn try_get_plugin_version(sEnv: *mut super::Environment, file: *const c_char, buffer: *mut c_char, buffer_size: i32) -> i32;
        pub fn init_sourcepawn_environment() -> *mut super::Environment;
    }
}