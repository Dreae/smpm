use comrak::{parse_document, format_html, ComrakOptions};
use comrak::nodes::{AstNode, NodeValue};
use typed_arena::Arena;
use std::mem;

#[inline]
pub fn escape_html(text: &[u8]) -> String {
    let mut output = String::with_capacity(text.len() * 2);

    for c in text.iter() {
        match c {
            &38 => output.push_str("&amp;"),
            &60 => output.push_str("&lt;"),
            &62 => output.push_str("&gt;"),
            &34 => output.push_str("&quot;"),
            &39 => output.push_str("&#x27;"),
            &47 => output.push_str("&#x2F;"),
            _ => output.push(*c as char)
        }
    };

    output
}

#[inline]
pub fn render_safe_markdown(text: &str) -> String {
    let arena = Arena::new();
    let root = parse_document(&arena, text, &ComrakOptions::default());

    iter_nodes(root, &|node| {
        match &mut node.data.borrow_mut().value {
            &mut NodeValue::HtmlBlock(ref mut block) => {
                block.literal = escape_html(&block.literal).as_bytes().to_vec();
            },
            &mut NodeValue::HtmlInline(ref mut text) => {
                let orig = mem::replace(text, vec![]);
                *text = escape_html(&orig).as_bytes().to_vec();
            },
            _ => ()
        }
    });

    let mut result = vec![];
    format_html(&root, &ComrakOptions::default(), &mut result).unwrap();

    String::from_utf8(result).unwrap()
}

fn iter_nodes<'a, F>(node: &'a AstNode<'a>, f: &F) where F: Fn(&'a AstNode<'a>) {
    f(node);
    for c in node.children() {
        iter_nodes(c, f);
    }
}