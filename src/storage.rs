use simple_s3::Client;
use env;

pub fn init_bucket(region: &str, bucket_name: &str) -> Client {
    let access_key = env::var("AWS_ACCESS_KEY_ID").expect("AWS Credentials not found");
    let secret_key = env::var("AWS_SECRET_ACCESS_KEY").expect("AWS Credentials not found");

    Client::with_endpoint("nyc3.digitaloceanspaces.com".to_owned(), bucket_name.to_owned(), region.to_owned(), access_key, secret_key)
}
