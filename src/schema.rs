table! {
    access_list (user_id, plugin_id) {
        user_id -> Int4,
        plugin_id -> Int4,
    }
}

table! {
    access_tokens (key_id) {
        key_id -> Varchar,
        token -> Varchar,
        user_id -> Int4,
        token_desc -> Text,
    }
}

table! {
    default_tokens (user_id, token_id) {
        user_id -> Int4,
        token_id -> Varchar,
    }
}

table! {
    use diesel_full_text_search::{TsVector as Tsvector};
    use diesel::sql_types::*;

    plugins (plugin_id) {
        plugin_id -> Int4,
        name -> Varchar,
        user_id -> Int4,
        private -> Bool,
        updated_date -> Timestamp,
        created_date -> Timestamp,
        downloads -> Int4,
        max_version -> Varchar,
        description -> Text,
        desc_searchable_col -> Tsvector,
    }
}

table! {
    users (user_id) {
        user_id -> Int4,
        username -> Varchar,
        email -> Varchar,
        password -> Varchar,
    }
}

table! {
    version_readme (readme_id) {
        readme_id -> Int4,
        version_id -> Int4,
        readme -> Text,
        date_rendered -> Timestamp,
    }
}

table! {
    versions (version_id) {
        version_id -> Int4,
        num -> Varchar,
        plugin_id -> Int4,
        created_date -> Timestamp,
        artifact_type -> Int4,
    }
}

joinable!(access_list -> plugins (plugin_id));
joinable!(access_list -> users (user_id));
joinable!(access_tokens -> users (user_id));
joinable!(default_tokens -> access_tokens (token_id));
joinable!(default_tokens -> users (user_id));
joinable!(plugins -> users (user_id));
joinable!(version_readme -> versions (version_id));
joinable!(versions -> plugins (plugin_id));

allow_tables_to_appear_in_same_query!(
    access_list,
    access_tokens,
    default_tokens,
    plugins,
    users,
    version_readme,
    versions,
);
