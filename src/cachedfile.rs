use rocket::response::{NamedFile, Responder, Result};
use rocket::{Request, Response};

pub struct CachedFile(pub NamedFile);

impl<'r> Responder<'r> for CachedFile {
    fn respond_to(self, req: &Request) -> Result<'r> {
        Response::build_from(self.0.respond_to(req)?)
            .raw_header("Cache-Control", "max-age=7200") //  24h (24*60*60)
            .ok()
    }
}