use reqwest;

pub struct ReCAPTCHA {
    pub site_key: String,
    site_secret: String,
    client: reqwest::Client
}

#[derive(Serialize)]
struct ReCAPTCHAValidateRequest<'a> {
    pub secret: &'a str,
    pub response: &'a str,
    pub remoteip: Option<&'a str>
}

#[derive(Deserialize)]
struct ReCAPTCHAValidateResponse {
    pub success: bool,
    pub challenge_ts: String,
    pub hostname: String,
    #[serde(rename = "error-codes")]
    pub error_codes: Option<Vec<String>>
}

impl ReCAPTCHA {
    pub fn new(site_key: String, site_secret: String) -> ReCAPTCHA {
        ReCAPTCHA {
            site_key,
            site_secret,
            client: reqwest::Client::new()
        }
    }

    pub fn validate_response(&self, client_response: &str, remote_ip: Option<&str>) -> bool {
        let req = ReCAPTCHAValidateRequest {
            secret: &self.site_secret,
            response: client_response,
            remoteip: remote_ip
        };

        let res = self.client.post("https://www.google.com/recaptcha/api/siteverify").form(&req).send();

        res.and_then(|mut response| {
            let parsed = response.json::<ReCAPTCHAValidateResponse>()?;

            Ok(parsed.success)
        }).unwrap_or(false)
    }
}