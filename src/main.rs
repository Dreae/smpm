#![feature(plugin, decl_macro, custom_derive, ptr_internals)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib;
#[macro_use] extern crate diesel;
#[macro_use(log)] extern crate log;
extern crate env_logger;
extern crate r2d2_diesel;
extern crate r2d2;
extern crate dotenv;
extern crate smpm_models;
extern crate simple_s3;
extern crate reqwest;
extern crate comrak;
extern crate typed_arena;
extern crate chrono;
extern crate ring;
extern crate hex;
extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate lazy_static;
extern crate diesel_full_text_search;
extern crate time;
extern crate multipart;
extern crate mkstemp;
extern crate zip;

use dotenv::dotenv;
use rocket_contrib::Template;
use ring::rand::SystemRandom;

use std::env;

mod cachedfile;
mod database;
mod models;
mod schema;
mod api;
mod error_pages;
mod storage;
mod ui;
mod markdown;
mod crypto;
mod smxvalidate;
mod url_generator;
mod recaptcha;

use recaptcha::ReCAPTCHA;

fn main() {
    dotenv().ok();

    env_logger::init();

    rocket::ignite()
        .manage(database::init_pool(&env::var("DATABASE_URL").expect("DATABASE_URL to be set")))
        .manage(storage::init_bucket(&env::var("S3_REGION").expect("S3_REGION to be set"), &env::var("S3_BUCKET").expect("S3_BUCKET to be set")))
        .manage(SystemRandom::new())
        .manage(ReCAPTCHA::new(env::var("RECAPTCHA_SITE_KEY").expect("Recaptcha key"), env::var("RECAPTCHA_SECRET_KEY").expect("Recaptcha key")))
        .mount("/", routes![
            ui::index,
            ui::files,
            // Plugins
            ui::search_plugins,
            ui::plugin_info,
            ui::get_newplugin,
            ui::post_newplugin,
            ui::get_plugin_edit,
            ui::get_plugin_permissions,
            ui::post_plugin_permissions,
            // Signup and login
            ui::get_signup,
            ui::post_signup,
            ui::get_login,
            ui::post_login,
            ui::logout,
            // Account control panel
            ui::get_account_cp,
            ui::get_account_tokens,
            ui::post_account_cp,
            ui::post_account_tokens
        ])
        .mount("/api", routes![
            api::plugin_info,
            api::plugin_info_byname,
            api::plugin_version_history,
            api::plugin_version_info,
            api::get_artifact
        ])
        .catch(errors![error_pages::not_found, error_pages::internal_server_error])
        .attach(Template::fairing())
        .launch();
}
