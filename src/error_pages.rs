use rocket_contrib::Template;
use rocket::Request;
use std::collections::HashMap;

#[error(404)]
pub fn not_found(_req: &Request) -> Template {
    Template::render("error_pages/404", HashMap::<String,String>::new())
}

#[error(500)]
pub fn internal_server_error(_req: &Request) -> Template {
    Template::render("error_pages/500", HashMap::<String,String>::new())
}