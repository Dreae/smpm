use rocket::response::Redirect;
use rocket::State;
use simple_s3::Client;
use diesel::prelude::*;

use ::database::DbConn;
use ::models::Plugin;
use ::schema::{plugins, versions};

use smpm_models::api::plugins::ArtifactType;

use super::{ApiError, Authorization};

#[get("/artifacts/<plugin_id>/<version_id>")]
pub fn get_artifact(conn: DbConn, authorization: Authorization, plugin_id: i32, version_id: i32, bucket: State<Client>) -> Result<Redirect, ApiError> {
    let target = Plugin::with_id(plugin_id, authorization.get_user_id());
    let plugin = plugins::table
        .filter(target)
        .select(plugins::plugin_id)
        .first::<i32>(&*conn);

    match plugin {
        Ok(_) => {
            let version = versions::table
                .filter(versions::version_id.eq(version_id))
                .select(versions::artifact_type)
                .first::<i32>(&*conn);

            if let Ok(version) = version {
                let artifact_name = if ArtifactType::from(version) == ArtifactType::Plugin {
                    "artifact.smx"
                } else {
                    "artifact.zip"
                };

                Ok(Redirect::found(&bucket.inner().presigned_url("GET", &format!("{}/{}/{}", plugin_id, version_id, artifact_name))))
            } else {
                Err(ApiError::NotFound)
            }
        },
        _ => Err(ApiError::NotFound)
    }
}