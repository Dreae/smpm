use diesel::prelude::*;
use rocket_contrib::Json;

use ::models::{Version, Plugin};
use ::schema::{versions, plugins};

use ::database::DbConn;
use smpm_models::api::plugins::{
    PluginManifest,
    PluginVersionHistory,
    PluginVersion,
    PluginVersionSummary
};
use super::ApiError;
use super::authentication::Authorization;

use ::url_generator::URLGenerator;

#[get("/plugins/<plugin_name>", rank = 2)]
pub fn plugin_info_byname(uri: URLGenerator, conn: DbConn, authorization: Authorization, plugin_name: String) -> Result<Json<PluginManifest>, ApiError> {
    let query = Plugin::with_name(&plugin_name, authorization.get_user_id());
    let plugin = plugins::table
        .filter(query)
        .select(Plugin::all_columns())
        .first::<Plugin>(&*conn);

    match plugin {
        Ok(plugin) => {
            let versions = versions::table
                .filter(versions::plugin_id.eq(plugin.plugin_id))
                .order(versions::created_date.desc())
                .load::<Version>(&*conn)
                .unwrap();

            Ok(Json(PluginManifest {
                plugin_id: plugin.plugin_id,
                name: plugin.name.clone(),
                author: uri.build_url(&format!("/api/users/{}", plugin.user_id)),
                version: versions.get(0).unwrap().num.clone(),
                versions: versions.iter().map(|version| {
                    version.num.clone()
                }).collect()
            }))
        },
        Err(_) => Err(ApiError::NotFound)
    }
}

#[get("/plugins/<plugin_id>")]
pub fn plugin_info(uri: URLGenerator, conn: DbConn, authorization: Authorization, plugin_id: i32) -> Result<Json<PluginManifest>, ApiError> {
    let query = Plugin::with_id(plugin_id, authorization.get_user_id());

    let plugin = plugins::table
        .filter(query)
        .select(Plugin::all_columns())
        .first::<Plugin>(&*conn);

    match plugin {
        Ok(plugin) => {
            let versions = versions::table
                .filter(versions::plugin_id.eq(plugin.plugin_id))
                .order(versions::created_date.desc())
                .load::<Version>(&*conn)
                .unwrap();

            Ok(Json(PluginManifest {
                plugin_id: plugin.plugin_id,
                name: plugin.name.clone(),
                author: uri.build_url(&format!("/api/users/{}", plugin.user_id)),
                version: versions.get(0).unwrap().num.clone(),
                versions: versions.iter().map(|version| {
                    version.num.clone()
                }).collect()
            }))
        },
        Err(_) => Err(ApiError::NotFound)
    }
}

#[get("/plugins/<plugin_id>/versions")]
pub fn plugin_version_history(uri: URLGenerator, conn: DbConn, authorization: Authorization, plugin_id: i32) -> Result<Json<PluginVersionHistory>, ApiError> {
    let target = Plugin::with_id(plugin_id, authorization.get_user_id());
    let plugin = plugins::table
        .filter(target)
        .select(Plugin::all_columns())
        .first::<Plugin>(&*conn);
    match plugin {
        Ok(plugin) => {
            let versions = versions::table
                .filter(versions::plugin_id.eq(plugin.plugin_id))
                .order(versions::created_date.desc())
                .load::<Version>(&*conn)
                .unwrap();

            Ok(Json(PluginVersionHistory {
                versions: versions.into_iter().map(|version| PluginVersionSummary {
                    num: version.num.clone(),
                    version: uri.build_url(&format!("/api/plugins/{}/versions/{}", plugin.plugin_id, version.version_id))
                }).collect()
            }))
        },
        _ => Err(ApiError::NotFound)
    }
}

#[get("/plugins/<plugin_id>/versions/<version_id>")]
pub fn plugin_version_info(uri: URLGenerator, conn: DbConn, authorization: Authorization, plugin_id: i32, version_id: i32) -> Result<Json<PluginVersion>, ApiError> {
    let target = Plugin::with_id(plugin_id, authorization.get_user_id());
    let plugin = plugins::table
        .filter(target)
        .select(plugins::plugin_id)
        .first::<i32>(&*conn);
    match plugin {
        Ok(_) => {
            let version = versions::table
                .filter(
                    versions::version_id.eq(version_id)
                )
                .first::<Version>(&*conn);

            match version {
                Ok(version) => {
                    Ok(Json(PluginVersion {
                        id: version.version_id,
                        num: version.num,
                        artifact: uri.build_url(&format!("/api/artifacts/{}/{}", plugin_id, version.version_id)),
                        date: version.created_date.timestamp(),
                        artifact_type: version.artifact_type.into()
                    }))
                },
                _ => Err(ApiError::NotFound)
            }
        },
        _ => Err(ApiError::NotFound)
    }
}