use rocket::request::{self, FromRequest, Request};
use rocket::outcome::Outcome;
use rocket::http::Status;
use diesel::prelude::*;
use log;

use ::database::DbConn;
use ::schema::access_tokens;
use ::models::AccessKey;
use ::crypto::verify_request;

pub enum Authorization {
    Token(i32),
    Unauthorized
}

impl Authorization {
    pub fn get_user_id(&self) -> Option<i32> {
        match self {
            &Authorization::Token(user_id) => Some(user_id),
            _ => None
        }
    }
}

impl <'a, 'r> FromRequest<'a, 'r> for Authorization {
    type Error = ();
    fn from_request(req: &'a Request<'r>) -> request::Outcome<Authorization, Self::Error> {
        let auth_id = req.headers().get_one("X-SMPM-KEY-ID");
        let signature = req.headers().get_one("X-SMPM-SIGNATURE");
        match (auth_id, signature) {
            (Some(key_id), Some(signature)) => {
                let conn = DbConn::from_request(req)?;
                let auth_key = access_tokens::table
                    .filter(access_tokens::key_id.eq(key_id))
                    .first::<AccessKey>(&*conn);
                match auth_key {
                    Ok(key) => {
                        if verify_request(req, &key.token, &signature) {
                            log::debug!("User {} authenticated by signed API request", key.user_id);
                            Outcome::Success(Authorization::Token(key.user_id))
                        } else {
                            Outcome::Failure((Status::Unauthorized, ()))
                        }
                    },
                    Err(_) => Outcome::Failure((Status::Unauthorized, ()))
                }
            },
            _ => {
                Outcome::Success(Authorization::Unauthorized)
            }
        }
    }
}