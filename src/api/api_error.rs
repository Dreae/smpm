use rocket::{Response, Request};
use rocket::response::Responder;
use rocket::http::{ContentType, Status};

#[derive(Debug)]
pub enum ApiError {
    NotFound
}

macro_rules! api_error {
    ($status:path, $msg:expr) => {
        Response::build()
            .header(ContentType::new("application", "json"))
            .status($status)
            .finalize()
    };
}

impl <'r> Responder<'r> for ApiError {
    fn respond_to(self, _request: &Request) -> Result<Response<'r>, Status> {
        match self {
            ApiError::NotFound => {
                Ok(api_error!(Status::NotFound, "Not found"))
            }
        }
    }
}
