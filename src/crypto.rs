use rocket::Request;
use ring::{digest, hmac, rand, pbkdf2};
use hex;

#[inline]
pub fn verify_request<'r>(req: &Request<'r>, key: &str, signature: &str) -> bool {
    verify_signature(req.uri().as_str(), key, signature)
}

#[inline]
pub fn verify_signature(value: &str, key: &str, signature: &str) -> bool {
    let key_bytes = hex::decode(key).unwrap();
    let signature_bytes = hex::decode(signature).unwrap();
    let verification_key = hmac::VerificationKey::new(&digest::SHA256, &key_bytes[..]);

    match hmac::verify(&verification_key, value.as_bytes(), &signature_bytes) {
        Ok(_) => true,
        Err(_) => false
    }
}

#[inline]
pub fn sign_str(value: &str, key: &str) -> String {
    let key_bytes = hex::decode(key).unwrap();
    let signing_key = hmac::SigningKey::new(&digest::SHA256, &key_bytes[..]);

    let signature = hmac::sign(&signing_key, &value.as_bytes());

    hex::encode(signature.as_ref())
}

#[inline]
pub fn gen_key(rng: &rand::SecureRandom) -> (String, String) {
    let mut key_bytes = [0u8; digest::SHA256_OUTPUT_LEN];
    let mut key_id = [0u8; 8];
    rng.fill(&mut key_id).unwrap();

    hmac::SigningKey::generate_serializable(&digest::SHA256, rng, &mut key_bytes).unwrap();

    (hex::encode(&key_id[..]), hex::encode(&key_bytes[..]))
}

#[inline]
pub fn pw_hash(password: &str, rng: &rand::SecureRandom) -> String {
    let mut salt = [0u8; 16];
    let mut hash = [0u8; digest::SHA512_256_OUTPUT_LEN];
    rng.fill(&mut salt).unwrap();
    pbkdf2::derive(&digest::SHA512_256, 100_000, &salt, password.as_bytes(), &mut hash);

    format!("{}:{}", hex::encode(hash), hex::encode(salt))
}

#[inline]
pub fn pw_verify(password: &str, hash: &str) -> bool {
    let vec = hash.split(":").collect::<Vec<&str>>();
    if vec.len() != 2 {
        return false;
    }
    let hash = vec.get(0).map(|h| hex::decode(h).unwrap());
    let salt = vec.get(1).map(|s| hex::decode(s).unwrap());

    match (salt, hash) {
        (Some(salt), Some(hash)) => {
            pbkdf2::verify(&digest::SHA512_256, 100_000, &salt, password.as_bytes(), &hash).is_ok()
        },
        _ => false
    }
}