use chrono::NaiveDateTime;
use diesel::prelude::*;
use diesel::pg::Pg;
use diesel::BoxableExpression;
use diesel::sql_types::Bool;
use diesel_full_text_search::*;
use serde::Serializer;

use ::schema::{plugins, access_list};

pub fn serialize_naive_datetime<S>(date_time: &NaiveDateTime, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
    serializer.serialize_str(&date_time.format("%b %-d, %Y").to_string())
}

#[derive(Queryable, Serialize)]
pub struct Plugin {
    pub plugin_id: i32,
    pub name: String,
    pub user_id: i32,
    pub private: bool,
    #[serde(serialize_with = "serialize_naive_datetime")]
    pub updated_date: NaiveDateTime,
    #[serde(serialize_with = "serialize_naive_datetime")]
    pub created_date: NaiveDateTime,
    pub downloads: i32,
    pub max_version: String,
    pub description: String
}

pub type PluginColumns = (
    plugins::plugin_id,
    plugins::name,
    plugins::user_id,
    plugins::private,
    plugins::updated_date,
    plugins::created_date,
    plugins::downloads,
    plugins::max_version,
    plugins::description
);

pub const ALL_COLUMNS: PluginColumns = (
    plugins::plugin_id,
    plugins::name,
    plugins::user_id,
    plugins::private,
    plugins::updated_date,
    plugins::created_date,
    plugins::downloads,
    plugins::max_version,
    plugins::description
);

impl Plugin {
    pub fn canonicalize_name(name: &str) -> Option<String> {
        if name.len() < 1 {
            return None;
        }

        let mut chars = name.chars();

        if !chars.next().unwrap().is_alphabetic() {
            return None;
        }

        for c in chars {
            if !c.is_alphanumeric() && c != '_' {
                return None;
            }
        }

        Some(name.to_lowercase())
    }

    pub fn with_name<'a>(plugin_name: &'a str, user_id: Option<i32>) -> Box<BoxableExpression<plugins::table, Pg, SqlType = Bool> + 'a> {
        Box::new(
            plugins::name.eq(plugin_name).and(
                Plugin::plugins_user_can_access(user_id)
            )
        )
    }

    pub fn with_id(plugin_id: i32, user_id: Option<i32>) -> Box<BoxableExpression<plugins::table, Pg, SqlType = Bool> + 'static> {
        Box::new(
            plugins::plugin_id.eq(plugin_id).and(
                Plugin::plugins_user_can_access(user_id)
            )
        )
    }

    pub fn search<'a>(q: &'a str, user_id: Option<i32>) -> Box<BoxableExpression<plugins::table, Pg, SqlType = Bool> + 'a> {
        let tsq = plainto_tsquery(q);

        Box::new(
            tsq.matches(plugins::desc_searchable_col).or(plugins::name.eq(q)).and(
                Plugin::plugins_user_can_access(user_id)
            )
        )
    }

    pub fn plugins_user_can_access(user_id: Option<i32>) -> Box<BoxableExpression<plugins::table, Pg, SqlType = Bool>> {
        if let Some(user_id) = user_id {
            Box::new(
                plugins::plugin_id.eq_any(
                    access_list::table
                        .filter(
                            access_list::user_id.eq(user_id)
                        )
                        .select(access_list::plugin_id)
                ).or(plugins::user_id.eq(user_id).or(plugins::private.eq(false)))
            )
        } else {
            Box::new(plugins::private.eq(false))
        }
    }

    pub fn all_columns() -> PluginColumns {
        ALL_COLUMNS
    }
}

#[derive(Insertable)]
#[table_name="plugins"]
pub struct NewPlugin {
    pub name: String,
    pub user_id: i32,
    pub private: bool,
    pub max_version: String,
    pub description: String
}

#[derive(Insertable, Queryable)]
#[table_name="access_list"]
pub struct PluginAccess {
    pub user_id: i32,
    pub plugin_id: i32
}