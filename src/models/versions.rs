use chrono::NaiveDateTime;
use ::schema::{versions, version_readme};

#[derive(Queryable)]
pub struct Version {
    pub version_id: i32,
    pub num: String,
    pub plugin_id: i32,
    pub created_date: NaiveDateTime,
    pub artifact_type: i32
}

#[derive(Insertable)]
#[table_name="versions"]
pub struct NewVersion {
    pub num: String,
    pub plugin_id: i32,
    pub artifact_type: i32
}

#[derive(Queryable)]
pub struct VersionReadme {
    pub readme_id: i32,
    pub version_id: i32,
    pub readme: String,
    pub date_rendered: NaiveDateTime
}

#[derive(Insertable)]
#[table_name="version_readme"]
pub struct NewReadme {
    pub version_id: i32,
    pub readme: String
}