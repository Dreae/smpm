use ::schema::users;
use ::schema::access_tokens;
use ::schema::default_tokens;

#[derive(Queryable, AsChangeset, Serialize)]
pub struct User {
    pub user_id: i32,
    pub username: String,
    pub email: String,
    pub password: String
}

#[derive(Insertable)]
#[table_name="users"]
pub struct NewUser<'a> {
    pub username: &'a str,
    pub password: &'a str,
    pub email: &'a str
}

#[derive(Queryable, Insertable, Serialize)]
#[table_name="access_tokens"]
pub struct AccessKey {
    pub key_id: String,
    pub token: String,
    pub user_id: i32,
    pub token_desc: String
}

#[derive(Queryable, Insertable)]
#[table_name="default_tokens"]
pub struct DefaultToken {
    pub token_id: String,
    pub user_id: i32
}