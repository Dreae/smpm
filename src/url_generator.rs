use rocket::request::{self, Request, FromRequest};
use rocket::outcome::Outcome;

pub struct URLGenerator {
    host: String,
    proto: String
}

impl URLGenerator {
    pub fn new(host: String, proto: String) -> URLGenerator {
        URLGenerator {
            host,
            proto
        }
    }

    pub fn build_url(&self, path: &str) -> String {
        format!("{}://{}{}", self.proto, self.host, path)
    }
}

impl <'a, 'r> FromRequest<'a, 'r> for URLGenerator {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<URLGenerator, Self::Error> {
        let headers = request.headers();
        let host = headers.get_one("host").unwrap_or_default();
        let proto = headers.get_one("x-forwarded-proto").unwrap_or("http");

        Outcome::Success(URLGenerator::new(host.to_owned(), proto.to_owned()))
    }
}