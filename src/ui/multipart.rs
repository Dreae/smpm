use rocket::request::Request;
use rocket::http::Status;
use rocket::data::{Data, DataStream, FromData, Outcome as DataOutcome};
use rocket::outcome::Outcome;
use multipart::server::{HttpRequest, Multipart};

pub struct MultipartRequest {
    boundary: String,
    data: Data
}

impl HttpRequest for MultipartRequest {
    type Body = DataStream;

    fn multipart_boundary(&self) -> Option<&str> {
        Some(&self.boundary)
    }

    fn body(self) -> Self::Body {
        self.data.open()
    }
}

impl FromData for MultipartRequest {
    type Error = ();
    fn from_data(request: &Request, data: Data) -> DataOutcome<Self, Self::Error> {
        if let Some(content_type) = request.content_type() {
            for (name, value) in content_type.params() {
                if name == "boundary" {
                    return Outcome::Success(MultipartRequest {
                        boundary: value.to_owned(),
                        data
                    });
                }
            }
        }

        Outcome::Failure((Status::BadRequest, ()))
    }
}

pub trait FromMultipart {
    fn from_multipart(form: Multipart<DataStream>) -> Self;
}
