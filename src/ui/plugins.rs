use std::io::{Read, Seek, SeekFrom};
use std::fs::{self, File};
use std::ops::Deref;

use rocket_contrib::Template;
use rocket::response::{Redirect, Failure};
use rocket::data::{Data, FromData, DataStream};
use rocket::request::Form;
use rocket::http::Status;
use diesel::prelude::*;
use diesel::PgConnection;
use chrono::NaiveDateTime;
use multipart::server::{Multipart, SaveResult};
use mkstemp::TempFile;
use rocket::{self, Request, State};
use simple_s3::Client;
use zip::ZipArchive;
use zip::read::ZipFile;
use reqwest;
use serde_json;
use diesel;
use log;

use diesel_full_text_search::*;

use ::smxvalidate;
use ::database::DbConn;
use ::models::{
    Plugin,
    PluginAccess,
    Version,
    NewPlugin,
    NewVersion,
    NewReadme,
    serialize_naive_datetime,
    User
};
use ::markdown;
use super::authentication::Authorization;
use super::{CurrentUserInfo, get_user_info};
use super::multipart::{MultipartRequest, FromMultipart};

use ::schema::{plugins, users, versions, version_readme, access_list};
use smpm_models::archive::ArchiveManifest;
use smpm_models::api::plugins::ArtifactType;

#[derive(Serialize)]
pub struct PluginInfo {
    pub name: String,
    pub author: String,
    pub description: String,
    pub version: String,
    pub versions: Vec<String>,
    pub downloads: i32,
    #[serde(serialize_with = "serialize_naive_datetime")]
    pub created_date: NaiveDateTime,
    pub readme: String,
    pub own_plugin: bool
}

#[derive(FromForm)]
pub struct SearchQuery {
    query: String
}

#[derive(Serialize)]
pub struct PluginResult {
    pub name: String,
    pub author: String,
    pub description: String,
    pub version: String,
    pub downloads: i32
}

#[derive(Serialize)]
pub struct SearchResults {
    pub matching_plugins: Vec<PluginResult>,
    pub user: Option<CurrentUserInfo>
}

#[get("/search?<query>")]
pub fn search_plugins(conn: DbConn, auth: Authorization, query: SearchQuery) -> Template {
    let search = Plugin::search(&query.query, auth.get_user_id());

    let q = plainto_tsquery(&query.query);

    let plugin_query = plugins::table.into_boxed();
    let matching_plugins = plugin_query
        .filter(search)
        .inner_join(users::table)
        .select((Plugin::all_columns(), users::username))
        .order(ts_rank_cd(plugins::desc_searchable_col, q))
        .limit(25)
        .load::<(Plugin, String)>(&*conn);

    let mut ctx = SearchResults {
        matching_plugins: vec![],
        user: None
    };

    if let Authorization::Cookie(user_id) = auth {
        ctx.user = Some(get_user_info(&*conn, user_id));
    }

    match matching_plugins {
        Ok(plugins) => {
            let plugin_results = plugins.into_iter().map(|result| {
                let (plugin, user) = result;
                PluginResult {
                    name: plugin.name,
                    author: user,
                    description: plugin.description,
                    version: plugin.max_version,
                    downloads: plugin.downloads
                }
            }).collect();

            ctx.matching_plugins = plugin_results;
        },
        Err(_) => { }
    }

    Template::render("search_results", &ctx)
}

#[get("/plugins/<plugin_name>")]
pub fn plugin_info(conn: DbConn, auth: Authorization, plugin_name: String) -> Option<Template> {
    let user_id = auth.get_user_id();
    let query = Plugin::with_name(&plugin_name, user_id);

    let plugin_query = plugins::table.into_boxed();
    let plugin = plugin_query
        .filter(query)
        .inner_join(users::table)
        .select((Plugin::all_columns(), (users::username, users::user_id)))
        .first::<(Plugin, (String, i32))>(&*conn);

    match plugin {
        Ok((plugin, user)) => {
            let versions = versions::table
                .filter(versions::plugin_id.eq(plugin.plugin_id))
                .inner_join(version_readme::table)
                .select((versions::all_columns, version_readme::readme))
                .order(versions::created_date.desc())
                .limit(15)
                .load::<(Version, String)>(&*conn)
                .unwrap();

            let latest_version = versions.get(0).unwrap();

            let plugin_info = PluginInfo {
                name: plugin.name,
                author: user.0.clone(),
                description: plugin.description,
                readme: latest_version.1.clone(),
                version: plugin.max_version,
                downloads: plugin.downloads,
                created_date: plugin.created_date,
                versions: versions.iter().map(|version| version.0.num.clone()).collect(),
                own_plugin: user_id.map(|id| id == user.1).unwrap_or(false)
            };
            Some(Template::render("plugin", &plugin_info))
        },
        Err(_) => None
    }
}

#[get("/plugins/<plugin_name>/edit")]
pub fn get_plugin_edit(conn: DbConn, auth: Authorization, plugin_name: String) -> Result<Template, Failure> {
    let plugin = load_own_plugin(&*conn, &plugin_name, auth.get_user_id())?;
    Ok(Template::render("plugins/new_version_form", &plugin))
}

#[derive(Serialize)]
pub struct AccessListData {
    pub allowed_users: Vec<User>,
    pub plugin: Plugin
}

#[derive(FromForm)]
pub struct UpdatePermissionsRequest {
    pub add_user: Option<String>,
    pub username: Option<String>,
    pub revoke_user: Option<String>,
    pub user_id: Option<i32>
}

#[get("/plugins/<plugin_name>/permissions")]
pub fn get_plugin_permissions(conn: DbConn, auth: Authorization, plugin_name: String) -> Result<Template, Failure> {
    let plugin = load_own_plugin(&*conn, &plugin_name, auth.get_user_id())?;
    let data = AccessListData {
        allowed_users: load_access_list(&*conn, plugin.plugin_id)?,
        plugin: plugin
    };

    Ok(Template::render("plugins/access_list", &data))
}

#[post("/plugins/<plugin_name>/permissions", data = "<req_data>")]
pub fn post_plugin_permissions(conn: DbConn, auth: Authorization, plugin_name: String, req_data: Form<UpdatePermissionsRequest>) -> Result<Template, Failure> {
    let plugin = load_own_plugin(&*conn, &plugin_name, auth.get_user_id())?;

    let form_values = req_data.into_inner();
    if form_values.add_user.is_some() && form_values.username.is_some() {
        let user = users::table
            .filter(users::username.eq(form_values.username.unwrap()))
            .select(users::user_id)
            .first::<i32>(&*conn);
        if let Ok(user_id) = user {
            let new_access = PluginAccess {
                user_id,
                plugin_id: plugin.plugin_id
            };

            diesel::insert_into(access_list::table).values(&new_access).execute(&*conn).unwrap();
        }
    } else if form_values.revoke_user.is_some() && form_values.user_id.is_some() {
        diesel::delete(
            access_list::table.filter(
                access_list::user_id.eq(
                    form_values.user_id.unwrap()
                ).and(access_list::plugin_id.eq(plugin.plugin_id))
            )
        ).execute(&*conn).unwrap();
    }

    let data = AccessListData {
        allowed_users: load_access_list(&*conn, plugin.plugin_id)?,
        plugin: plugin
    };

    Ok(Template::render("plugins/access_list", &data))
}

fn load_own_plugin(conn: &PgConnection, plugin_name: &str, user_id: Option<i32>) -> Result<Plugin, Failure> {
    match user_id {
        Some(user_id) => {
            let query = Plugin::with_name(&plugin_name, Some(user_id));

            let plugin = plugins::table
                .filter(query)
                .select(Plugin::all_columns())
                .first::<Plugin>(conn);

            match plugin {
                Ok(plugin) => {
                    if user_id != plugin.user_id {
                        Err(Failure(Status::NotFound))
                    } else {
                        Ok(plugin)
                    }
                },
                _ => Err(Failure(Status::NotFound))
            }
        },
        None => Err(Failure(Status::Unauthorized))
    }
}

fn load_access_list(conn: &PgConnection, plugin_id: i32) -> Result<Vec<User>, Failure> {
    let users = access_list::table
        .filter(access_list::plugin_id.eq(plugin_id))
        .inner_join(users::table)
        .select((access_list::user_id, users::all_columns))
        .load::<(i32, User)>(conn);

    match users {
        Ok(results) => {
            Ok(results.into_iter().map(|result| result.1).collect())
        },
        _ => Err(Failure(Status::InternalServerError))
    }
}

#[derive(Serialize)]
pub struct NewPluginCtx {
    user: Option<CurrentUserInfo>,
    errors: Option<NewPluginErrors>
}

#[derive(Serialize, Default)]
pub struct NewPluginErrors {
    plugin_file: bool,
    plugin_archive: bool,
    plugin_name: bool,
    no_plugin_name: bool,
    no_plugin_description: bool,
    no_plugin_file: bool
}

impl NewPluginErrors {
    fn new() -> NewPluginErrors {
        Default::default()
    }

    fn plugin_file_invalid(&mut self) {
        self.plugin_file = true;
    }

    fn plugin_archive_invalid(&mut self) {
        self.plugin_archive = true;
    }

    fn plugin_name_invalid(&mut self) {
        self.plugin_name = true;
    }

    fn plugin_name_required(&mut self) {
        self.no_plugin_name = true;
    }

    fn plugin_desc_required(&mut self) {
        self.no_plugin_description = true;
    }

    fn plugin_file_required(&mut self) {
        self.no_plugin_file = true;
    }
}

#[get("/newplugin")]
pub fn get_newplugin(conn: DbConn, auth: Authorization) -> Result<Template, Redirect> {
    let mut ctx = NewPluginCtx {
        user: None,
        errors: None
    };

    if let Authorization::Cookie(user_id) = auth {
        ctx.user = Some(get_user_info(&*conn, user_id));
    } else {
        return Err(Redirect::found("/login"));
    }

    Ok(Template::render("new_plugin", &ctx))
}

#[derive(Default)]
pub struct PluginUploadRequest {
    pub plugin_file: Option<TempFile>,
    pub plugin_archive: Option<TempFile>,
    pub plugin_name: Option<String>,
    pub plugin_description: Option<String>,
    pub plugin_readme: Option<String>,
    pub plugin_private: bool
}

impl FromMultipart for PluginUploadRequest {
    fn from_multipart(mut form: Multipart<DataStream>) -> PluginUploadRequest {
        let mut req: PluginUploadRequest = Default::default();

        log::debug!("Generating PluginUploadRequest");
        form.foreach_entry(|mut entry| {
            match entry.headers.name.deref().deref() {
                "plugin_name" => {
                    let mut value = String::new();
                    if entry.data.read_to_string(&mut value).is_ok() {
                        if value.len() != 0 {
                            req.plugin_name = Some(value);
                        }
                    }
                },
                "plugin_description" => {
                    let mut value = String::new();
                    if entry.data.read_to_string(&mut value).is_ok() {
                        if value.len() != 0 {
                            req.plugin_description = Some(value);
                        }
                    }
                },
                "plugin_readme" => {
                    let mut value = String::new();
                    if entry.data.read_to_string(&mut value).is_ok() {
                        if value.len() != 0 {
                            req.plugin_readme = Some(value);
                        }
                    }
                },
                "plugin_file" => {
                    if let Ok(mut tempfile) = TempFile::new("/tmp/upload.plugin.XXXXXX", true) {
                        if let SaveResult::Full(bytes) = entry.data.save().write_to(&mut tempfile) {
                            if bytes != 0 {
                                req.plugin_file = Some(tempfile);
                            }
                        }
                    }
                },
                "plugin_archive" => {
                    log::debug!("Got plugin_archive upload");
                    if let Ok(mut tempfile) = TempFile::new("/tmp/upload.archive.XXXXXX", true) {
                        if let SaveResult::Full(bytes) = entry.data.save().write_to(&mut tempfile) {
                            if bytes != 0 {
                                req.plugin_archive = Some(tempfile);
                            }
                        }
                    }
                },
                _ => { }
            }
        }).unwrap();

        req
    }
}

impl FromData for PluginUploadRequest {
    type Error = ();
    fn from_data(request: &Request, data: Data) -> rocket::data::Outcome<Self, Self::Error> {
        let req = MultipartRequest::from_data(request, data)?;

        match Multipart::from_request(req) {
            Ok(form) => rocket::Outcome::Success(PluginUploadRequest::from_multipart(form)),
            Err(_) => rocket::Outcome::Failure((rocket::http::Status::BadRequest, ()))
        }
    }
}

struct Artifact {
    artifact_type: ArtifactType,
    temp_file: TempFile
}

#[post("/newplugin", data = "<data>")]
pub fn post_newplugin(conn: DbConn, auth: Authorization, data: PluginUploadRequest, bucket: State<Client>) -> Result<Redirect, Template> {
    let mut ctx = NewPluginCtx {
        user: None,
        errors: None
    };

    let authed_user;
    if let Authorization::Cookie(user_id) = auth {
        authed_user = user_id;
        ctx.user = Some(get_user_info(&*conn, user_id));
    } else {
        return Ok(Redirect::found("/login"));
    }

    let new_plugin = process_new_plugin(authed_user, data);

    match new_plugin {
        Ok((plugin, readme, artifact)) => {
            // TODO: Clean up these `unwrap`s
            let plugin_id = diesel::insert_into(plugins::table).values(&plugin).returning(plugins::plugin_id).get_result::<i32>(&*conn).unwrap();

            let new_version = NewVersion {
                num: plugin.max_version,
                plugin_id: plugin_id,
                artifact_type: artifact.artifact_type.into_int()
            };
            let version_id = diesel::insert_into(versions::table).values(&new_version).returning(versions::version_id).get_result::<i32>(&*conn).unwrap();

            let new_readme = NewReadme {
                version_id,
                readme
            };
            diesel::insert_into(version_readme::table).values(&new_readme).execute(&*conn).unwrap();

            let artifact_name = match artifact.artifact_type {
                ArtifactType::Plugin => "artifact.smx",
                _ => "artifact.zip"
            };

            let client = reqwest::Client::new();

            let metadata = fs::metadata(artifact.temp_file.path()).unwrap();
            let len = metadata.len();

            let body = reqwest::Body::new(artifact.temp_file);
            let storage_path = format!("{}/{}/{}", plugin_id, version_id, artifact_name);

            client.put(&bucket.deref().presigned_url("PUT", &storage_path))
                .header(reqwest::header::ContentLength(len))
                .body(body)
                .send().unwrap();

            Ok(Redirect::found(&format!("/plugins/{}", plugin.name)))
        },
        Err(errors) => {
            ctx.errors = Some(errors);
            Err(Template::render("new_plugin", &ctx))
        }
    }

}


// TODO: This needs major refactoring
fn process_new_plugin(user_id: i32, req: PluginUploadRequest) -> Result<(NewPlugin, String, Artifact), NewPluginErrors> {
    let mut errors: NewPluginErrors = NewPluginErrors::new();
    let new_plugin = if let Some(tempfile) = req.plugin_file {
        if let Ok(plugin_version) = smxvalidate::try_get_plugin_version(tempfile.path()) {
            log::info!("SMX validation passed, plugin version: {}", plugin_version);

            if req.plugin_name.is_some() && req.plugin_description.is_some() {
                if let Some(canon_name) = Plugin::canonicalize_name(&req.plugin_name.unwrap()) {
                    let new_plugin = NewPlugin {
                        name: canon_name,
                        user_id: user_id,
                        private: req.plugin_private,
                        max_version: plugin_version,
                        description: req.plugin_description.unwrap()
                    };

                    let readme = markdown::render_safe_markdown(&req.plugin_readme.unwrap_or_default());
                    let artifact = Artifact {
                        artifact_type: ArtifactType::Plugin,
                        temp_file: tempfile
                    };

                    Some((new_plugin, readme, artifact))
                } else {
                    errors.plugin_name_invalid();
                    None
                }
            } else {
                if req.plugin_name.is_none() {
                    errors.plugin_name_required();
                }

                if req.plugin_description.is_none() {
                    errors.plugin_desc_required();
                }

                None
            }
        } else {
            log::info!("SMX validation failed, plugin file invalid");
            errors.plugin_file_invalid();
            None
        }
    } else if let Some(mut tempfile) = req.plugin_archive {
        let archive_contents = if let Ok(mut zipfile) = ZipArchive::new(tempfile.inner()) {
            validate_plugin_archive(zipfile, user_id)
        } else {
            None
        };

        if let Some((plugin, readme)) = archive_contents {
            let artifact = Artifact {
                artifact_type: ArtifactType::Archive,
                temp_file: tempfile
            };

            Some((plugin, readme, artifact))
        } else {
            errors.plugin_archive_invalid();
            None
        }
    } else {
        errors.plugin_file_required();
        None
    };

    match new_plugin {
        Some((new_plugin, readme, mut artifact)) => {
            artifact.temp_file.inner().seek(SeekFrom::Start(0)).unwrap();
            Ok((new_plugin, readme, artifact))
        },
        _ => Err(errors)
    }
}

fn validate_plugin_archive(mut archive: ZipArchive<&mut File>, user_id: i32) -> Option<(NewPlugin, String)> {
    match archive.by_name("manifest.json") {
        Ok(manifest_file) => {
            let new_plugin = serde_json::from_reader::<ZipFile, ArchiveManifest>(manifest_file).map(|manifest| {
                NewPlugin {
                    name: manifest.name,
                    user_id,
                    private: manifest.private,
                    max_version: manifest.version,
                    description: manifest.description,
                }
            });

            if new_plugin.is_err() {
                None
            } else {
                Some(new_plugin.unwrap())
            }
        },
        _ => None
    }.map(|new_plugin| {
        let readme = match archive.by_name("readme.md") {
            Ok(mut readme) => {
                let mut buf = String::new();
                readme.read_to_string(&mut buf).map(|_| {
                    Some(markdown::render_safe_markdown(&buf))
                }).unwrap_or(None)
            },
            _ => None
        };

        (new_plugin, readme.unwrap_or_default())
    })
}