use std::collections::HashMap;

use diesel;
use diesel::prelude::*;
use rocket_contrib::Template;
use rocket::response::{Redirect, Failure};
use rocket::request::Form;
use rocket::http::{Cookies, Cookie, Status};
use rocket::State;
use ring::rand::SystemRandom;
use serde_json;
use time::Duration;

use super::authentication::{AuthCookie, AUTH_COOKIE_NAME};
use super::authentication::Authorization;

use ::schema::{users, access_tokens, default_tokens};
use ::database::DbConn;
use ::models::{User, NewUser, AccessKey, DefaultToken};
use ::recaptcha::ReCAPTCHA;
use ::crypto;

#[derive(Serialize, FromForm)]
pub struct SignupRequest {
    pub username: String,
    pub password: String,
    pub password_confirm: String,
    pub email: String,
    #[form(field = "g-recaptcha-response")]
    pub recaptcha_response: String
}

#[derive(Serialize, FromForm)]
pub struct LoginRequest {
    pub username: String,
    pub password: String,
    pub remember_me: bool
}

#[derive(Serialize, Default)]
pub struct SignupState<'a> {
    pub username: Option<&'a str>,
    pub password: Option<&'a str>,
    pub email: Option<&'a str>,
    pub recaptcha_site_key: &'a str
}

#[get("/signup")]
pub fn get_signup(captcha_client: State<ReCAPTCHA>) -> Template {
    let mut state: SignupState = Default::default();
    state.recaptcha_site_key = &captcha_client.site_key;

    Template::render("signup", &state)
}

#[post("/signup", data = "<request>")]
pub fn post_signup(rng: State<SystemRandom>, conn: DbConn, request: Form<SignupRequest>, captcha_client: State<ReCAPTCHA>) -> Result<Redirect, Template> {
    let form_values = request.get();

    let mut state: SignupState = Default::default();
    state.username = Some(&form_values.username);
    state.password = Some(&form_values.password);
    state.email = Some(&form_values.email);
    state.recaptcha_site_key = &captcha_client.site_key;

    if form_values.password != form_values.password_confirm {
        return Err(Template::render("signup", &state));
    }

    if !captcha_client.validate_response(&form_values.recaptcha_response, None) {
        return Err(Template::render("signup", &state));
    }

    let pw_hash = crypto::pw_hash(&form_values.password, &*rng);
    let new_user = NewUser {
        username: &form_values.username,
        password: &pw_hash,
        email: &form_values.email
    };

    let result = diesel::insert_into(users::table)
        .values(&new_user)
        .returning(users::user_id)
        .get_result::<i32>(&*conn);

    match result {
        Ok(user_id) => {
            let (key_id, key) = crypto::gen_key(&*rng);
            let access_key = AccessKey {
                key_id: key_id.clone(),
                token: key,
                user_id,
                token_desc: "Default access token".to_owned()
            };
            let default_key = DefaultToken {
                token_id: key_id,
                user_id
            };

            diesel::insert_into(access_tokens::table)
                .values(&access_key)
                .execute(&*conn)
                .unwrap();

            diesel::insert_into(default_tokens::table)
                .values(&default_key)
                .execute(&*conn)
                .unwrap();

            Ok(Redirect::found("/"))
        },
        Err(_) => Err(Template::render("signup", &state))
    }

}

#[get("/login")]
pub fn get_login() -> Template {
    Template::render("login", &HashMap::<&str, String>::new())
}

#[post("/login", data = "<form>")]
pub fn post_login(conn: DbConn, rng: State<SystemRandom>, form: Form<LoginRequest>, mut cookies: Cookies) -> Result<Redirect, Template> {
    let form_values = form.get();
    let user = users::table
        .filter(users::username.eq(&form_values.username))
        .first::<User>(&*conn);

    match user {
        Ok(user) => {
            if crypto::pw_verify(&form_values.password, &user.password) {
                let token = default_tokens::table
                    .filter(default_tokens::user_id.eq(user.user_id))
                    .inner_join(access_tokens::table)
                    .select(access_tokens::all_columns)
                    .first::<AccessKey>(&*conn)
                    .unwrap();

                let auth_cookie = AuthCookie::new(token.key_id, &token.token, &*rng);
                let mut cookie = Cookie::build(AUTH_COOKIE_NAME, serde_json::to_string(&auth_cookie).unwrap())
                    .http_only(true)
                    .path("/")
                    .finish();

                if form_values.remember_me {
                    cookie.set_max_age(Duration::days(60));
                }

                cookies.add(cookie);

                Ok(Redirect::found("/"))
            } else {
                Err(Template::render("login", form_values))
            }
        },
        _ => {
            Err(Template::render("login", form_values))
        }
    }
}

#[post("/logout")]
pub fn logout(mut cookies: Cookies) -> Redirect {
    cookies.remove(Cookie::named(AUTH_COOKIE_NAME));

    Redirect::found("/")
}

#[get("/account")]
pub fn get_account_cp(conn: DbConn, auth: Authorization) -> Result<Template, Result<Redirect, Failure>> {
    render_account_cp("account_details", conn, auth, None, None)
}

#[get("/account/tokens")]
pub fn get_account_tokens(conn: DbConn, auth: Authorization) -> Result<Template, Result<Redirect, Failure>> {
    render_account_cp("account_tokens", conn, auth, None, None)
}

#[derive(FromForm)]
pub struct AccountSubmitRequest {
    pub email: String,
    pub current_password: String,
    pub new_password: String,
    pub password_confirm: String
}

#[post("/account", data = "<req_data>")]
pub fn post_account_cp(conn: DbConn, auth: Authorization, req_data: Form<AccountSubmitRequest>, rng: State<SystemRandom>) -> Result<Template, Result<Redirect, Failure>> {
    if auth == Authorization::Unauthorized {
        return Err(Ok(Redirect::found("/login")));
    }
    let user_id = auth.get_user_id().unwrap();

    let form_data = req_data.into_inner();
    let user = users::table
        .filter(users::user_id.eq(user_id))
        .first::<User>(&*conn);

    match user {
        Ok(mut user) => {
            let target = users::table.filter(users::user_id.eq(user.user_id));
            let mut errors: AccountInfoErrors = Default::default();
            if form_data.email != "" {
                user.email = form_data.email;
            }

            if form_data.new_password != "" {
                if crypto::pw_verify(&form_data.current_password, &user.password) {
                    if form_data.new_password == form_data.password_confirm {
                        user.password = crypto::pw_hash(&form_data.new_password, &*rng);
                    } else {
                        errors.password_mismatch = true;
                    }
                } else {
                    errors.old_password_invalid = true;
                }
            }

            diesel::update(target).set(&user).execute(&*conn).unwrap();

            render_account_cp("account_details", conn, auth, None, Some(errors))
        },
        _ => Err(Ok(Redirect::found("/login")))
    }
}

#[derive(FromForm)]
pub struct TokenSubmitRequest {
    pub key_id: Option<String>,
    pub regenerate: Option<String>,
    pub delete: Option<String>,
    pub key_desc: Option<String>,
    pub new_token: Option<String>
}

#[post("/account/tokens", data = "<req_data>")]
pub fn post_account_tokens(conn: DbConn, auth: Authorization, req_data: Form<TokenSubmitRequest>, rng: State<SystemRandom>) -> Result<Template, Result<Redirect, Failure>> {
    if auth == Authorization::Unauthorized {
        return Err(Ok(Redirect::found("/login")));
    }
    let user_id = auth.get_user_id().unwrap();

    let form_data = req_data.into_inner();
    let mut show_key = None;
    if form_data.new_token.is_some() && form_data.key_desc.is_some() {
        let (key_id, key) = crypto::gen_key(&*rng);
        let access_key = AccessKey {
            key_id: key_id.clone(),
            token: key,
            user_id,
            token_desc: form_data.key_desc.unwrap()
        };

        diesel::insert_into(access_tokens::table)
            .values(&access_key)
            .execute(&*conn)
            .unwrap();

        show_key = Some(key_id);
    } else if form_data.delete.is_some() && form_data.key_id.is_some() {
        diesel::delete(access_tokens::table.filter(access_tokens::key_id.eq(form_data.key_id.unwrap())))
            .execute(&*conn)
            .unwrap();
    } else if form_data.regenerate.is_some() && form_data.key_id.is_some() && form_data.key_desc.is_some() {
        let old_key_id = form_data.key_id.unwrap();
        let num_deleted = diesel::delete(default_tokens::table.filter(default_tokens::token_id.eq(&old_key_id)))
            .execute(&*conn)
            .unwrap();
        diesel::delete(access_tokens::table.filter(access_tokens::key_id.eq(&old_key_id)))
            .execute(&*conn)
            .unwrap();

        let (key_id, key) = crypto::gen_key(&*rng);
        let access_key = AccessKey {
            key_id: key_id.clone(),
            token: key,
            user_id,
            token_desc: form_data.key_desc.unwrap()
        };

        diesel::insert_into(access_tokens::table)
            .values(&access_key)
            .execute(&*conn)
            .unwrap();

        if num_deleted > 0 {
            let default_key = DefaultToken {
                token_id: key_id.clone(),
                user_id
            };

            diesel::insert_into(default_tokens::table)
                .values(&default_key)
                .execute(&*conn)
                .unwrap();
        }

        show_key = Some(key_id);
    }

    render_account_cp("account_tokens", conn, auth, show_key, None)
}

#[derive(Serialize, Default)]
pub struct AccountInfoErrors {
    pub old_password_invalid: bool,
    pub password_mismatch: bool,
    pub not_an_email: bool
}

#[derive(Serialize)]
pub struct AccountInfo {
    pub username: String,
    pub email: String,
    pub default_token: String,
    pub access_tokens: Vec<AccessKey>,
    pub show_key: Option<String>,
    pub errors: AccountInfoErrors
}

fn render_account_cp(template: &'static str, conn: DbConn, auth: Authorization, show_key: Option<String>, errors: Option<AccountInfoErrors>) -> Result<Template, Result<Redirect, Failure>> {
    if auth == Authorization::Unauthorized {
        return Err(Ok(Redirect::found("/login")));
    }
    let user_id = auth.get_user_id().unwrap();

    let user = users::table
        .filter(users::user_id.eq(user_id))
        .inner_join(default_tokens::table)
        .select((users::all_columns, default_tokens::token_id))
        .first::<(User, String)>(&*conn);
    let access_tokens = access_tokens::table
        .filter(access_tokens::user_id.eq(user_id))
        .load::<AccessKey>(&*conn);

    match (user, access_tokens) {
        (Ok((user, default_token)), Ok(tokens)) => {
            let info = AccountInfo {
                username: user.username,
                email: user.email,
                default_token: default_token,
                access_tokens: tokens,
                show_key,
                errors: errors.unwrap_or_default()
            };

            Ok(Template::render(template, &info))
        },
        _ => Err(Err(Failure::from(Status::InternalServerError)))
    }
}