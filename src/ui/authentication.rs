use std::time::UNIX_EPOCH;

use diesel::prelude::*;
use rocket::request::{self, Request, FromRequest};
use rocket::outcome::Outcome;
use ring::rand::SecureRandom;
use rocket::http::Cookie;
use serde_json;
use hex;
use log;

use ::database::DbConn;
use ::models::AccessKey;
use ::schema::access_tokens;
use ::crypto;

pub static AUTH_COOKIE_NAME: &'static str = "smpm_auth";

#[derive(PartialEq)]
pub enum Authorization {
    Cookie(i32),
    Unauthorized
}

impl Authorization {
    pub fn get_user_id(&self) -> Option<i32> {
        match self {
            &Authorization::Cookie(user_id) => Some(user_id),
            _ => None
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct AuthCookie {
    pub key_id: String,
    pub created: u64,
    pub csrf_token: String,
    pub signature: String
}

impl AuthCookie {
    pub fn new(key_id: String, token: &str, rng: &SecureRandom) -> AuthCookie {
        let created = UNIX_EPOCH.elapsed().unwrap().as_secs();

        let mut csrf_bytes = [0u8; 12];
        rng.fill(&mut csrf_bytes).unwrap();
        let csrf_token = hex::encode(csrf_bytes);

        let signature = crypto::sign_str(&format!("{}:{}:{}", key_id, csrf_token, created), token);
        AuthCookie {
            key_id,
            created,
            csrf_token,
            signature
        }
    }
}

macro_rules! validation_failed {
    ($cookies:ident) => {
        $cookies.remove(Cookie::named(AUTH_COOKIE_NAME));
        return Outcome::Success(Authorization::Unauthorized);
    };
}

impl <'a, 'r> FromRequest<'a, 'r> for Authorization {
    type Error = ();
    fn from_request(req: &'a Request<'r>) -> request::Outcome<Authorization, Self::Error> {
        let mut cookies = req.cookies();
        let cookie = match cookies.get(AUTH_COOKIE_NAME) {
            Some(cookie) => cookie.value().to_owned(),
            None => return Outcome::Success(Authorization::Unauthorized)
        };

        let auth_cookie = serde_json::from_str::<AuthCookie>(&cookie);
        match auth_cookie {
            Ok(cookie_val) => {
                let conn = DbConn::from_request(req)?;
                let token = access_tokens::table
                    .filter(access_tokens::key_id.eq(&cookie_val.key_id))
                    .first::<AccessKey>(&*conn);
                if token.is_err() {
                    validation_failed!(cookies);
                }

                let token = token.unwrap();

                if !crypto::verify_signature(&format!("{}:{}:{}", cookie_val.key_id, cookie_val.csrf_token, cookie_val.created), &token.token, &cookie_val.signature) {
                    validation_failed!(cookies);
                } else {
                    log::debug!("User {} authenticated by cookie", token.user_id);
                    Outcome::Success(Authorization::Cookie(token.user_id))
                }
            },
            Err(_) => {
                validation_failed!(cookies);
            }
        }
    }
}
