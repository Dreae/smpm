use std::path::{Path, PathBuf};

use rocket::response::NamedFile;
use rocket_contrib::Template;
use diesel::prelude::*;
use diesel::pg::PgConnection;
use ::cachedfile::CachedFile;

mod plugins;
mod authentication;
mod users;
mod multipart;

pub use self::plugins::*;
pub use self::users::*;

use ::schema::{plugins as plugins_schema, users as users_schema};
use ::models::Plugin;
use ::database::DbConn;

use self::authentication::Authorization;

macro_rules! fetch_plugin_list {
    ($order_by:expr, $conn:ident) => {
        plugins_schema::table
            .filter(plugins_schema::private.eq(false))
            .order($order_by)
            .limit(20)
            .select(Plugin::all_columns())
            .load::<Plugin>(&*$conn)
            .unwrap_or_default()
    }
}

#[derive(Serialize)]
pub struct CurrentUserInfo {
    pub username: String,
    pub plugins: Vec<Plugin>
}

#[derive(Serialize)]
pub struct HomepageCtx {
    newest: Vec<Plugin>,
    updated: Vec<Plugin>,
    downloaded: Vec<Plugin>,
    user: Option<CurrentUserInfo>
}

#[get("/")]
pub fn index(conn: DbConn, auth: Authorization) -> Template {
    let newest = fetch_plugin_list!(plugins_schema::created_date.desc(), conn);
    let updated = fetch_plugin_list!(plugins_schema::updated_date.desc(), conn);
    let downloaded = fetch_plugin_list!(plugins_schema::downloads.desc(), conn);

    let mut ctx = HomepageCtx {
        newest,
        updated,
        downloaded,
        user: None
    };

    if let Authorization::Cookie(user_id) = auth {
        ctx.user = Some(get_user_info(&*conn, user_id));
    }

    Template::render("homepage", &ctx)
}

#[get("/static/<file..>")]
pub fn files(file: PathBuf) -> Option<CachedFile> {
    NamedFile::open(Path::new("static/").join(file)).ok().map(|f| CachedFile(f))
}

pub fn get_user_info(conn: &PgConnection, user_id: i32) -> CurrentUserInfo {
    let username = users_schema::table
        .filter(users_schema::user_id.eq(user_id))
        .select(users_schema::username)
        .first::<String>(conn)
        .unwrap();

    let top_plugins = plugins_schema::table
        .filter(plugins_schema::user_id.eq(user_id))
        .order(plugins_schema::downloads.desc())
        .limit(6)
        .select(Plugin::all_columns())
        .load::<Plugin>(&*conn)
        .unwrap_or_default();

    CurrentUserInfo {
        username,
        plugins: top_plugins
    }
}